module fpgatest (out,clk);
// Lチカするプログラム。
//　module名はプロジェクト名と合わせる。
// out,clk(clock)は引数。
	input clk;
	output out;
	reg[24:0]count;

// 25ビット分のreg（変数の型）を記憶できるメモリを定義する。
// 1本分の入力と出力がある回路を作る。
// 出力はLEDにつながる。
// すべての動作はクロックに同期される。
// 今回は振動数50MHz
// 水晶発振子
	assign out = count[24];
// カウントという25bitの出力を使う。
// 25ビットのうち1ビット（最上位ビット）をLEDへの出力に使う。
// posedge 立ち上がり
// negaedge 立ち下り
// 相手側と通信するような場合は、ここを使い分ける。
// チカチカの周期を早くしたい場合は、regを24→23にする。
// PLL？
	always @(posedge(clk)) begin
		count <= count + 1;
	end
endmodule